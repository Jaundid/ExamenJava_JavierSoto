/**
 * 
 */
package Clases;

/**
 * @author Usuario
 *
 */
public class Empleado {

	public String nombre;
	public String apellido;
	public String cedula;
	public int edad;
	public boolean casado;
	public double salario;
	
	
	
	
	public Empleado(String nombre, String apellido, String cedula, int edad, boolean casado, double salario) {
		this.nombre = nombre;
		this.apellido = apellido;
		this.cedula = cedula;
		//this.edad = edad;
		if (this.edad<18 && this.edad>45){
			this.edad=18;
		}else{
			this.edad = edad;
		}
	
		this.casado = casado;
		this.salario = salario;
		
		//aqu� indico que si no est� en el rango, por defecto ser� 18
		
}
	public Empleado(){
		this.nombre=null;
		this.apellido = null;
		this.cedula = null;
		this.edad = 18;
		this.casado = false;
		this.salario = 0;
	}
	
	
	public void clasificacionEdad(){
		if (getEdad()<21){
			System.out.println("Principiante");
		}
		if (getEdad()>=22 && getEdad()<=35){
			System.out.println("Intermedio");
		}
		if (getEdad()<35){
			System.out.println("Senior");
		}
		
	}
	
	public double aumentarSalario( double digitos)
	{
		
		return getSalario()+(getSalario()*digitos/100);
	}
		
		
	

	public String getNombre() {
		return nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public String getCedula() {
		return cedula;
	}

	public int getEdad() {
		return edad;
	}

	public boolean isCasado() {
		return casado;
	}

	public double getSalario() {
		return salario;
	}
	
	
	
	
	
}
