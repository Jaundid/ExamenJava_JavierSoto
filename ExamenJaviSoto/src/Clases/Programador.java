package Clases;

public class Programador extends Empleado {
	
	public String lenguajeDominante;
	public int lineasDeCodigoPorHora;
	
	public Programador() {
		super();
		this.lenguajeDominante=null;
		this.lineasDeCodigoPorHora=0;
		// TODO Auto-generated constructor stub
	}
	
	
	public Programador(String nombre, String apellido, String cedula, int edad, boolean casado, double salario, int codigo, String lenguaje)
	{
		super(nombre,apellido,cedula,edad,casado,salario);
		this.lenguajeDominante=lenguaje;
		this.lineasDeCodigoPorHora=codigo;
		
	}


	public String getLenguajeDominante() {
		return lenguajeDominante;
	}


	public int getLineasDeCodigoPorHora() {
		return lineasDeCodigoPorHora;
	}




	
	
	
	
	
	
	
	
	
}
